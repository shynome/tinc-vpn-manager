import *as React from 'react'
import {
  Typography,
  AppBar, Toolbar,
  IconButton,
  LinearProgress,
} from "@material-ui/core";
import { Add as AddIcon, Refresh as RefreshIcon } from "@material-ui/icons";

import { Query } from "@/Common/GraphQL";
import ShowMessage from "@/Components/ShowMessage";
import { Tinc } from '@/Common/Tinc';
import AddHost from './AddHost'

export interface State {
  hosts: Query['hosts']
  AddHostOpen: boolean
  ShowHostname: string
  errMsg: string
  updateHostsLock: boolean
}

import { HostList } from './HostList'
export class Home extends React.Component<{},State> {
  constructor(props:any){
    super(props)
    this.state = {
      hosts: [],
      errMsg: '',
      AddHostOpen: false,
      ShowHostname: '',
      updateHostsLock: true,
    }
    this.initPromise = this.init()
  }
  initPromise: Promise<any>
  async init(){
    await Promise.all([
      this.setHosts(),
    ])
  }
  async setHosts(){
    const tinc = await Tinc.get()
    let hosts = await tinc.getHosts()
    this.setState({ hosts, updateHostsLock: false })
  }
  _updateHosts = async ()=>{
    const tinc = await Tinc.get()
    await tinc.updateHosts()
    await this.setHosts()
  }
  updateHosts = async ()=>{
    if(this.state.updateHostsLock){ return }
    await new Promise(rl=>this.setState({ updateHostsLock: true }, rl)) 
    await this._updateHosts().catch(
      err=>this.setState({ errMsg: err.message })
    )
    this.setState({ updateHostsLock: false })
  }
  renderHeader = ()=>{
    return (
      <AppBar position='static'>
        <Toolbar>
          <Typography variant='h6' color='inherit' style={{flexGrow:1}}>HOSTS</Typography>
          <IconButton color='inherit' onClick={ this.updateHosts }>
            <RefreshIcon />
          </IconButton>
          <IconButton color='inherit' onClick={ ()=>this.setState({ AddHostOpen: true }) }>
            <AddIcon />
          </IconButton>
        </Toolbar>
        <AddHost open={ this.state.AddHostOpen } onAdded={ this.onHostAdded } close={ ()=>this.setState({ AddHostOpen: false }) }></AddHost>
      </AppBar>
    )
  }
  onHostAdded = async ()=>{
    await this.updateHosts()
    this.setState({ AddHostOpen: false })
  }
  render(){
    return (
      <div>
        { this.renderHeader() }
        { this.state.updateHostsLock && <LinearProgress /> }
        <HostList hosts={ this.state.hosts }></HostList>
        <ShowMessage message={ this.state.errMsg }></ShowMessage>
      </div>
    )
  }
}