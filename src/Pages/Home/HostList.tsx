
import *as React from "react";

import {
  List, ListItem, ListItemText, ListItemSecondaryAction,
  IconButton,
  Button,
  Dialog, DialogContent, DialogContentText, DialogActions, DialogTitle,
  LinearProgress,
  Slide,
} from "@material-ui/core";

export const DialogTransitionUp:React.StatelessComponent<{}> = props=>(
  <Slide direction='up' {...props}></Slide>
)

import {
  Visibility as VisibilityIcon,
  Delete as DeleteIcon,
} from "@material-ui/icons";

import { ShowHost } from './ShowHost'
import { errors } from '@/subjects'
import { Tinc } from "@/Common/Tinc";

export interface Props {
  hosts: string[]
}
export interface State {
  ShowHostname: string
  DeleteHostname: string
  DeleteHostnameLock: boolean
}

export class HostList extends React.Component<Props,State> {
  state:State = {
    ShowHostname: '',
    DeleteHostname: '',
    DeleteHostnameLock: false,
  }
  renderItem = (hostname:string)=>{
    return (
      <ListItem key={hostname}>
        <ListItemText primary={ hostname }></ListItemText>
        <ListItemSecondaryAction>
          <IconButton onClick={ ()=>this.setState({ ShowHostname: hostname }) }>
            <VisibilityIcon />
          </IconButton>
          <IconButton onClick={ ()=>this.setState({ DeleteHostname: hostname }) }>
            <DeleteIcon />
          </IconButton>
        </ListItemSecondaryAction>
      </ListItem>
    )
  }
  _deleteHost = async (hostname:string)=>{
    const tinc = await Tinc.get()
    await tinc.delHost(hostname)
  }
  deleteHost = async ()=>{
    if(this.state.DeleteHostnameLock){ return }
    await new Promise(rl=>this.setState({ DeleteHostnameLock: true },rl))
    await this._deleteHost(this.state.DeleteHostname)
    .catch( errors.next )
    this.setState({
      DeleteHostnameLock: false,
      DeleteHostname: '',
    })
  }
  render(){
    return (
      <>
        <List>
          { this.props.hosts.map(this.renderItem) }
        </List>
        <ShowHost
          open={ !!this.state.ShowHostname }
          close={ ()=>this.setState({ ShowHostname: '' }) } 
          hostname={ this.state.ShowHostname } 
          TransitionComponent={DialogTransitionUp}
          />
        <Dialog open={ !!this.state.DeleteHostname } TransitionComponent={DialogTransitionUp}>
          <DialogTitle>是否删除 {this.state.DeleteHostname}</DialogTitle>
          <DialogContent>
            <DialogContentText>
              确认是否删除 {this.state.DeleteHostname} 的 HOST 文件, 一旦删除无法找回, 必须重新添加
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button color='primary' disabled={ this.state.DeleteHostnameLock } onClick={ ()=>this.setState({ DeleteHostname: '' }) } >
              取消
            </Button>
            <Button color='primary' disabled={ this.state.DeleteHostnameLock } onClick={ this.deleteHost } >
              <LinearProgress />
              确认
            </Button>
          </DialogActions>
        </Dialog>
      </>
    )
  }
}
