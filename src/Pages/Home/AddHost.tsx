import *as React from 'react'

import {
  Dialog, DialogContent, DialogTitle, DialogActions, 
  Button,  
  TextField,
} from "@material-ui/core";
import { Tinc } from '@/Common/Tinc'

export interface Props {
  onAdded: ()=>void
  open: boolean
  close: ()=>void
}

export interface State {
  hostname: string
  content: string
}

export default class AddHost extends React.Component<Props,State> {
  constructor(props:any){
    super(props)
    this.state = {
      hostname: '',
      content: '',
    }
  }
  addHost = async ()=>{
    const tinc = await Tinc.get()
    let { hostname, content } = this.state
    await tinc.addHost(hostname, content)
    this.props.onAdded()
  }
  render(){
    return (
      <Dialog open={ this.props.open } fullScreen={ window.outerWidth < 600 } >
        <DialogTitle>添加新的主机</DialogTitle>
        <DialogContent>
          <TextField 
            label='主机名'
            onChange={ e=>this.setState({ hostname: e.target.value }) }
            required
            fullWidth
            variant='outlined'
            margin='normal'
          />
          <TextField 
            label='主机公钥'
            onChange={ e=>this.setState({ content: e.target.value }) }
            required
            multiline
            rows={ 15 }
            variant='outlined'
            fullWidth
            margin='normal'
          />
        </DialogContent>
        <DialogActions>
          <Button color='primary' onClick={ this.props.close }>
            取消
          </Button>
          <Button color='primary' disabled={ !this.state.content || !this.state.hostname } onClick={ this.addHost } >
            添加
          </Button>
        </DialogActions>
      </Dialog>
    )
  }
} 
