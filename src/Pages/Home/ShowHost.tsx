import *as React from 'react'

import {
  Dialog, DialogContent, DialogTitle, DialogActions,
  Button, 
  LinearProgress,
  Typography,
} from "@material-ui/core";
import { Tinc } from "@/Common/Tinc";

export interface Props {
  open: boolean
  close: ()=>void
  hostname: string
  TransitionComponent?: React.ReactType
}

export interface State {
  content: string
}

export class ShowHost extends React.Component<Props,State> {
  constructor(props:Props){
    super(props)
    this.state = {
      content: ''
    }
    this.componentWillReceiveProps(props)
  }
  componentWillReceiveProps(props:Props){
    if(!props.hostname){ return }
    this.setContent()
  }
  setContent = async ()=>{
    const tinc = await Tinc.get()
    let content = await tinc.getHost(this.props.hostname)
    this.setState({
      content,
    })
  }
  render(){
    return (
      <Dialog open={ this.props.open } TransitionComponent={ this.props.TransitionComponent }>
        <DialogTitle>{this.props.hostname} 的 HOST 文件</DialogTitle>
        <DialogContent>
          {
            this.state.content
            ? <Typography component='pre'>{this.state.content}</Typography>
            : <LinearProgress />
          }
        </DialogContent>
        <DialogActions>
          <Button fullWidth color='primary' onClick={ this.props.close } >
            关闭
          </Button>
        </DialogActions>
      </Dialog>
    )
  }
}

export default ShowHost
