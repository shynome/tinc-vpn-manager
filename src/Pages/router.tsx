import *as React from 'react'
import { Switch, Route, Redirect, } from 'react-router'

import { NotFound } from './NotFound'
import { Home } from './Home'

export const Routes:Array<[string,any]> = [
  [ '/', ()=><Redirect to="/home" /> ],
  [ '/home', Home ],
  [ '/404', NotFound ],
]

export const SwitchRouter:React.StatelessComponent<any> = props =>{
  const RouteComponents = Routes.map(([path, Component])=>{
    return <Route exact={ true }  key={ path } path={ path } component={ Component } ></Route>
  })
  return (
    <Switch>
      { RouteComponents }
      <Route component={ NotFound } />
    </Switch>
  )
}