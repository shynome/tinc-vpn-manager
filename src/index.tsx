import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

const render = ()=>{
  ReactDOM.render(
    <App />,
    document.getElementById('root') as HTMLElement
  );
}

registerServiceWorker();

render()

if(module.hot){
  module.hot.accept('./App',()=>{
    render()
  })
}
