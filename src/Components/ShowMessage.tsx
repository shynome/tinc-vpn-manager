import *as React from 'react'
import {
  Slide,
  Snackbar,
} from "@material-ui/core";

const SnackbarTransition:React.StatelessComponent<{}> = props=>(
  <Slide direction='up' {...props}></Slide>
)

export interface Props {
  message: string,
}

export interface State {
  timeoutId: number
  message: string
}

export default class ShowMessage extends React.Component<Props,State> {
  constructor(props:any){
    super(props)
    this.state = {
      timeoutId: 0,
      message: '',
    }
    this.initShowTip()
  }
  initShowTip(){
    if(!this.props.message){ return }
    clearTimeout(this.state.timeoutId)
    let timeoutId = setTimeout(()=>{
      this.setState({ timeoutId: 0 })
    },2000) as any as number
    this.setState({
      timeoutId,
      message: this.props.message,
    })
  }
  componentWillReceiveProps(){
    this.initShowTip()
  }
  render(){
    const { timeoutId, message } = this.state
    return (
      <Snackbar
        open={ !!(timeoutId && message) }
        message={ message }
        TransitionComponent={ SnackbarTransition }
      />
    )
  }
}
