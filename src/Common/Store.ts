import { createInstance } from "localforage";

export class Store<D> {
  private instance: LocalForage
  constructor(name:string){
    this.instance = createInstance({ name, })
  }
  get = async <k extends keyof D>(key:k)=>{
    return this.instance.getItem<D[k]>(key as string)
  }
  set = async <k extends keyof D>(key:k,value:D[k])=>{
    return this.instance.setItem(key as string,value)
  }
}
