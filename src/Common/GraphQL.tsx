export type Maybe<T> = T | null;

// ====================================================
// Types
// ====================================================

export interface Query {
  /** 主机列表 */
  hosts: string[];
  /** 服务器主机的host */
  host: string;
}

export interface Mutation {
  addHost: string;

  delHost: string;
}

// ====================================================
// Arguments
// ====================================================

export interface HostQueryArgs {
  /** 要输出的 host 名字 */
  hostname: string;
}
export interface AddHostMutationArgs {
  /** 主机名 */
  hostname: string;
  /** host 文件内容 */
  content: string;
  /** 是否覆盖同名文件 */
  force?: Maybe<boolean>;
}
export interface DelHostMutationArgs {
  /** 主机名 */
  hostname: string;
}

export interface IntrospectionResultData {
  __schema: {
    types: {
      kind: string;
      name: string;
      possibleTypes: {
        name: string;
      }[];
    }[];
  };
}

const result: IntrospectionResultData = {
  __schema: {
    types: []
  }
};

export default result;
