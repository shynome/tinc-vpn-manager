import { Store } from "./Store";
import { Query } from "./GraphQL";

export const config = new Store<{
  'token': string,
  'gql_endpoint': string,
  'hosts': Query['hosts'],
}>('config')
