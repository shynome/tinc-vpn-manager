import axios,{ AxiosInstance, AxiosPromise } from "axios";
import *as GQLType from "./GraphQL";
import { Store } from './Store'
import { config } from './config'

export const hostsStore = new Store<{
  [key:string]: GQLType.Query['host'],
}>('hosts')

export class Tinc {
  client:AxiosInstance
  constructor(url:string, token:string){
    this.client = axios.create({
      baseURL: url,
      headers: {
        token
      }
    })
  }
  static gql = {
    hosts: `
    query {
      hosts
    }
    `,
    host: `
    query getHost($hostname: String!){
      host(hostname: $hostname)
    }
    `,
    addHost: `
    mutation addHost($hostname: String!, $content: String!){
      addHost(hostname: $hostname, content: $content)
    }
    `,
    delHost: `
    mutation ($hostname: String!) {
      delHost(hostname: $hostname)
    }
    `,
  }
  async addHost(hostname:string, content:string){
    let result = await this.request<GQLType.Mutation>({
      mutation: Tinc.gql.addHost,
      variables: {
        hostname,
        content,
      },
    })
    return result.data
  }
  async delHost(hostname:string){
    let result = await this.request<GQLType.Mutation, GQLType.DelHostMutationArgs>({
      mutation: Tinc.gql.delHost,
      variables: {
        hostname,
      },
    })
    return result.data
  }
  async getHost(hostname:string){
    let result = await this.request<GQLType.Query, GQLType.HostQueryArgs>({
      query: Tinc.gql.host,
      variables: {
        hostname,
      },
    })
    return result.data.host
  }
  async updateHosts(){
    let result = await this.request<GQLType.Query>({
      query: Tinc.gql.hosts
    })
    let hosts = result.data.hosts
    await config.set('hosts',hosts)
    return hosts
  }
  async getHosts(){
    let hosts = await config.get('hosts')
    if(!hosts){
      hosts = await this.updateHosts()
    }
    return hosts
  }
  static _defaultTinc:Promise<Tinc>
  static async get(){
    if(this._defaultTinc){
      return this._defaultTinc
    }
    return this._defaultTinc = (async ()=>{
      let uri = await config.get('gql_endpoint')
      let token = await config.get('token')
      return new Tinc(uri, token)
    })()
  }
  request:<T,Y={}>(data:{ query?:string, mutation?:string, variables?:Y })=>AxiosPromise<T> = async (data)=>{
    let result =  await this.client.post('',{
      query: data.mutation || data.query,
      variables: data.variables,
    })
    return result.data
  }
}
