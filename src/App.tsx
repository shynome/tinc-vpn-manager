import * as React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { SwitchRouter } from './Pages/router';
import { config } from "@/Common/config";
import { 
  Typography,
  CircularProgress,
  Dialog, DialogContent, DialogTitle, DialogActions,
  Grid,
  TextField,
  Button,
  CssBaseline,
} from "@material-ui/core";
import ShowMessage from "@/Components/ShowMessage";
import { Tinc } from "@/Common/Tinc";

export interface State {
  init: boolean
  token: string
  gql_endpoint: string
  loginLock: boolean
  errMsg: string
  inputToken: string
}

export let tinc:Tinc

import { errors } from "@/subjects";

class App extends React.Component<{}, State> {
  constructor(props:any){
    super(props)
    this.state = {
      init: false,
      token: '',
      gql_endpoint: '',
      loginLock: false,
      errMsg: '',
      inputToken: '',
    }
    this.initPromise = this.init()
    errors.subscribe(
      err=>{

      }
    )
  }
  initPromise: Promise<void>
  async init(){
    let [ token, gql_endpoint ] = await Promise.all([
      config.get('token'),
      config.get('gql_endpoint'),
    ])
    this.setState({
      init: true,
      token: token || '',
      gql_endpoint: gql_endpoint || '',
    })
  }
  lastSetGQLEndpointPromise: Promise<any>
  setGQLEndpoint = (value:string)=>{
    this.setState({ gql_endpoint: value })
    this.lastSetGQLEndpointPromise = config.set('gql_endpoint', value);
  }
  async _login(){
    await this.lastSetGQLEndpointPromise
    let gql_endpoint = await config.get('gql_endpoint')
    let token = this.state.inputToken
    tinc = new Tinc( gql_endpoint, token)
    await tinc.getHosts()
    await config.set('token', token)
    this.setState({ token })
  }
  login = async ()=>{
    if(this.state.loginLock){
      return
    }
    await new Promise(rl=>this.setState({ loginLock: true }, rl))
    await this._login().catch(
      e=>this.setState({ errMsg: e.message })
    )
    this.setState({ loginLock: false })
  }
  renderLogin(){
    if(!this.state.init){
      return
    }
    return (
      <Dialog
        open={ !this.state.token }
      >
        <DialogTitle>设置服务器相关信息</DialogTitle>
        <DialogContent>
          <TextField 
            label='tinc vpn api 地址 (默认开头是 https:// 的)'
            type='url'
            placeholder='https://xxxxxxxxxxx/graphql'
            defaultValue= {this.state.gql_endpoint}
            onChange={ e=>this.setGQLEndpoint(e.target.value) }
            autoFocus
            fullWidth
            required
            margin='normal'
          />
          <TextField 
            label='接口认证使用的 Token'
            type='password'
            onChange={ e=>this.setState({ inputToken: e.target.value }) }
            required
            fullWidth
            margin='normal'
          />
        </DialogContent>
        <DialogActions>
          <Button color='primary' fullWidth onClick={ this.login }>
            登录
          </Button>
        </DialogActions>
      </Dialog>
    )
  }
  renderLoding(){
    return (
      <Dialog fullScreen open={ !this.state.init } >
        <Grid
          container
          direction="column"
          justify="center"
          alignItems="center"
          style={{height:'100%'}}
        >
          <Grid item>
            <CircularProgress />
          </Grid>
        </Grid>
      </Dialog>
    )
  }
  renderAuth(){
    return (
      <>
        {this.renderLogin()}
        {this.renderLoding()}
        <ShowMessage message={ this.state.errMsg }></ShowMessage>
      </>
    )
  }
  render() {
    return (
      <Typography component='div'>
        <CssBaseline />
        {
          this.state.token
          ? (
            <BrowserRouter>
              <SwitchRouter />
            </BrowserRouter>
          )
          : (
            this.renderAuth()
          )
        }
      </Typography>
    );
  }
}

export default App;
